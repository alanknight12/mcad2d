/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graficador;

import com.jogamp.opengl.GLCapabilities;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.swing.*;
import modelos.CirculoVac;
import modelos.Forma;

public class Panel extends JPanel{
    
    JPanel panelBotones;
    JButton btnCircLleno,btnCuadLleno,btnRecLleno,btnRomboLleno,btnTriLleno;
    JButton btnCircVac,btnCuadVac,btnRecVac,btnRomboVac,btnTriVac;
    JButton btnRotarIzq,btnRotarDer,btnEscalarXP,btnEscalarXN,btnEscalarYP,btnEscalarYN;
    JButton btnBorrar;
    PanelPintar panelPintar;
    GLCapabilities capabilities;
    
    public int selected;
    
    public Panel(GLCapabilities capabilities){
        this.capabilities = capabilities;
        init();
    }
    
    public void init() {
        this.setLayout(new BorderLayout());
        
        panelBotones = new JPanel();
        panelBotones.setLayout(new GridLayout(9, 2));
        
        panelPintar = new PanelPintar(capabilities);
        panelPintar.setSize(800, 600);
        
        btnCircLleno = new JButton("Circ LLeno");
        btnCircLleno.setName("cirL");
        
        btnCuadLleno = new JButton("Cuad LLeno");
        btnCuadLleno.setName("cuadL");
        
        btnRecLleno = new JButton("Pentagono LLeno");
        btnRecLleno.setName("recL");
        
        btnRomboLleno = new JButton("Rombo LLeno");
        btnRomboLleno.setName("romL");
        
        btnTriLleno = new JButton("Tri LLeno");
        btnTriLleno.setName("triL");
        
        btnCircVac = new JButton("Circ Vac");
        btnCircVac.setName("cirV");
        
        btnCuadVac = new JButton("Cuad Vac");
        btnCuadVac.setName("cuadV");
        
        btnRecVac = new JButton("Pentagono Vac");
        btnRecVac.setName("recV");
        
        btnRomboVac = new JButton("Rombo Vac");
        btnRomboVac.setName("romV");
        
        btnTriVac = new JButton("Tri Vac");
        btnTriVac.setName("triV");
        
        btnRotarIzq = new JButton("Rotar izq");
        btnRotarIzq.setName("rotIzq");
        btnRotarDer = new JButton("Rotar der");
        btnRotarDer.setName("rotDer");
        btnEscalarXP = new JButton("Escalar X +");
        btnEscalarXP.setName("escXP");
        btnEscalarXN = new JButton("Escalar X -");
        btnEscalarXN.setName("escXN");
        btnEscalarYP = new JButton("Escalar Y +");
        btnEscalarYP.setName("escYP");
        btnEscalarYN = new JButton("Escalar Y -");
        btnEscalarYN.setName("escYN");
        
        btnBorrar = new JButton("Borrar figura");
        btnBorrar.setName("borrar");
        
        
        panelBotones.add(btnCircLleno);
        panelBotones.add(btnCircVac);
        panelBotones.add(btnCuadLleno);
        panelBotones.add(btnCuadVac);
        panelBotones.add(btnRecLleno);
        panelBotones.add(btnRecVac);
        panelBotones.add(btnRomboLleno);
        panelBotones.add(btnRomboVac);
        panelBotones.add(btnTriLleno);
        panelBotones.add(btnTriVac);
        panelBotones.add(btnRotarIzq);
        panelBotones.add(btnRotarDer);
        panelBotones.add(btnEscalarXP);
        panelBotones.add(btnEscalarXN);
        panelBotones.add(btnEscalarYP);
        panelBotones.add(btnEscalarYN);
        panelBotones.add(btnBorrar);
        
        panelPintar.setBackground(Color.cyan);
        this.add(panelBotones,BorderLayout.WEST);
        this.add(panelPintar,BorderLayout.CENTER);
        
    }
    
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
    }
    
    public void addEventos(Controlador c) {
        panelPintar.addMouseMotionListener(c);
        panelPintar.addMouseListener(c);
        this.panelPintar.addEventos(c);
        
        btnCircVac.addActionListener(c);
        btnCircLleno.addActionListener(c);
        btnCuadVac.addActionListener(c);
        btnCuadLleno.addActionListener(c);
        btnRecVac.addActionListener(c);
        btnRecLleno.addActionListener(c);
        btnTriVac.addActionListener(c);
        btnTriLleno.addActionListener(c);
        btnRomboVac.addActionListener(c);
        btnRomboLleno.addActionListener(c);
        btnRotarIzq.addActionListener(c);
        btnRotarDer.addActionListener(c);
        btnEscalarXP.addActionListener(c);
        btnEscalarXN.addActionListener(c);
        btnEscalarYP.addActionListener(c);
        btnEscalarYN.addActionListener(c);
        btnBorrar.addActionListener(c);
    }
}


