/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.jogamp.opengl.GL2;
import java.awt.Color;
import java.awt.Graphics;

public class CirculoVac extends Forma {
    
    public CirculoVac(float lados, float grados, float x, float y, float radio, float r, float g, float b){
        this.NLados = lados;
        this.grados = grados;
        this.x = x;
        this.y = y;
        this.radio = 30;
        
        xs = 1;
        ys = 1;
        this.r = r;
        this.g = g;
        this.b = b;
    }
    
    
    public void dibujaFigura(GL2 gl) {
        
        gl.glLoadIdentity();
        
        gl.glTranslatef(x, y, 0);
        gl.glRotatef(grados, 0, 0, 1);
        gl.glScalef(xs, ys, 0f);
        
        gl.glColor3f(r, g, b);        
        
        float gra = 2;
        float inc = 2;
        float xf,yf,xa,ya;
        
        xa = radio*((float)Math.sin(Math.toRadians(0)));
        ya = radio*((float)Math.cos(Math.toRadians(0)));

        
        gl.glBegin(GL2.GL_LINES);
        
        for(int i=1;i<NLados;i++){
            xf = radio*((float)Math.sin(Math.toRadians(gra)));
            yf = radio*((float)Math.cos(Math.toRadians(gra)));
            gra+=inc;
            gl.glVertex3f(xa, ya, 0);
            gl.glVertex3f(xf, yf, 0);
            xa = xf;
            ya = yf;
        }

        gl.glEnd();
    }
    
    public boolean checa(int xe, int ye) {
        float dist = (float)(Math.sqrt((xe-x)*(xe-x)+(ye-y)*(ye-y)));
        if(dist<=radio)
            return true;
        return false;
    }
}