/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.jogamp.opengl.GL2;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class TrianguloLleno extends Forma{
    
    public TrianguloLleno(float lados, float grados, float x, float y, float radio, float r, float g, float b){
        this.NLados = lados;
        this.grados = grados;
        this.x = x;
        this.y = y;
        xs = 1;
        ys = 1;
        this.radio = 30;
        this.r = r;
        this.g = g;
        this.b = b;
    }
    
    
    public void dibujaFigura(GL2 gl) {
        
        gl.glLoadIdentity();
        
        gl.glTranslatef(x, y, 0);
        gl.glRotatef(grados, 0, 0, 1);
        gl.glScalef(xs, ys, 0f);

        gl.glBegin(GL2.GL_TRIANGLES);
        gl.glColor3f(r, g, b);
        
        float x1,y1,x2,y2,x3,y3;
        
        float inc = 120;
        
        x1 = (float)(radio*Math.sin(Math.toRadians(300)));
        y1 = (float)(radio*Math.cos(Math.toRadians(300)));
        
        x2 = (float)(radio*Math.sin(Math.toRadians(60)));
        y2 = (float)(radio*Math.cos(Math.toRadians(60)));
        
        x3 = (float)(radio*Math.sin(Math.toRadians(180)));
        y3 = (float)(radio*Math.cos(Math.toRadians(180)));
        
        
        gl.glVertex3f(x1, y1, 0);
        gl.glVertex3f(x2, y2, 0);
        gl.glVertex3f(x3, y3, 0);
        
        gl.glEnd();
        
        
    }
    
    public boolean checa(int xe, int ye) {
        float dist = (float)(Math.sqrt((xe-x)*(xe-x)+(ye-y)*(ye-y)));
        if(dist<=radio)
            return true;
        return false;
    }

}
