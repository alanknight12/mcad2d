/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.jogamp.opengl.GL2;
import java.awt.Color;
import java.awt.Graphics;

public class RomboVacio extends Forma{
    
    public RomboVacio(float lados, float grados, float x, float y, float radio, float r, float g, float b){
        this.NLados = lados;
        this.grados = grados;
        this.x = x;
        this.y = y;
        this.radio = 30;
        xs = 1;
        ys = 1;
        this.r = r;
        this.g = g;
        this.b = b;
    }
    
    
    public void dibujaFigura(GL2 gl) {
        
        gl.glLoadIdentity();
        
        gl.glTranslatef(x, y, 0);
        gl.glRotatef(grados, 0, 0, 1);
        gl.glScalef(xs, ys, 0f);
        
        gl.glColor3f(r, g, b);
        
        gl.glBegin(GL2.GL_LINES);
        
        float ptsX[] = new float[(int)NLados];
        float ptsY[] = new float[(int)NLados];
        
        float gra = 0;
        float inc = 90;
        float xf,yf,xa,ya;
        
        for(int i=0;i<NLados;i++){
            ptsX[i] = radio*((float)Math.sin(Math.toRadians(gra)));
            ptsY[i] = radio*((float)Math.cos(Math.toRadians(gra)));
            gra+=inc;
        }
        
        for(int i=1;i<NLados;i++){
            gl.glVertex3f(ptsX[i-1],ptsY[i-1],0);
            gl.glVertex3f(ptsX[i],ptsY[i],0);
        }
        
        gl.glVertex3f(ptsX[0],ptsY[0],0);
        gl.glVertex3f(ptsX[3],ptsY[3],0);

        gl.glEnd();
    }
    
    public boolean checa(int xe, int ye) {
        float dist = (float)(Math.sqrt((xe-x)*(xe-x)+(ye-y)*(ye-y)));
        if(dist<=radio)
            return true;
        return false;
    }

}
